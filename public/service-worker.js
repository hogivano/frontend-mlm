(() => {
    'use strict'

    const SW = {
        init () {
            self.addEventListener('push', this.notificationPush.bind(this));
            self.addEventListener('notificationclick', this.notificationClick.bind(this));
            self.addEventListener('notificationclose', this.notificationClose.bind(this));


            //Install stage sets up the offline page in the cache and opens a new cache
            self.addEventListener('install', function(event) {
                var offlinePage = new Request('offline.html');
                event.waitUntil(
                    fetch(offlinePage).then(function(response) {
                        return caches.open('pwabuilder-offline').then(function(cache) {
                            console.log('[PWA Builder] Cached offline page during Install'+ response.url);
                            return cache.put(offlinePage, response);
                        });
                    }));
            });

            //If any fetch fails, it will show the offline page.
            //Maybe this should be limited to HTML documents?
            self.addEventListener('fetch', function(event) {
                if (event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') {
                    event.respondWith(
                        fetch(event.request).catch(function (error) {
                                console.error('[PWA Builder] Network request Failed. Serving offline page ' + error);
                                return caches.open('pwabuilder-offline').then(function (cache) {
                                    return cache.match('offline.html');
                                });
                            }
                        ));
                }
            });

            //This is a event that can be fired from your page to tell the SW to update the offline page
            self.addEventListener('refreshOffline', function(response) {
                return caches.open('pwabuilder-offline').then(function(cache) {
                    console.log('[PWA Builder] Offline page updated from refreshOffline event: '+ response.url);
                    return cache.put(offlinePage, response);
                });
            });
        },

        /**
         * Handle notification push event.
         *
         * https://developer.mozilla.org/en-US/docs/Web/Events/push
         *
         * @param {NotificationEvent} event
         */
        notificationPush (event) {
            if (!(self.Notification && self.Notification.permission === 'granted')) {
                return
            }

            // https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData
            if (event.data) {
                event.waitUntil(
                    this.sendNotification(event.data.json())
                )
            }
        },

        /**
         * Handle notification click event.
         *
         * https://developer.mozilla.org/en-US/docs/Web/Events/notificationclick
         *
         * @param {NotificationEvent} event
         */
        notificationClick (event) {
            // console.log(event.notification)

            if (event.action === 'some_action') {
                // Do something...
            } else {
                self.clients.openWindow('/')
            }
        },

        /**
         * Handle notification close event (Chrome 50+, Firefox 55+).
         *
         * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/onnotificationclose
         *
         * @param {NotificationEvent} event
         */
        notificationClose (event) {
            self.registration.pushManager.getSubscription().then(subscription => {
                if (subscription) {
                    this.dismissNotification(event, subscription)
                }
            })
        },

        /**
         * Send notification to the user.
         *
         * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
         *
         * @param {PushMessageData|Object} data
         */
        sendNotification (data) {
            return self.registration.showNotification(data.title, data)
        },

        /**
         * Send request to server to dismiss a notification.
         *
         * @param  {NotificationEvent} event
         * @param  {String} subscription endpoint
         * @return {Response}
         */
        dismissNotification ({ notification }, { endpoint }) {
            if (!notification.data || !notification.data.id) {
                return
            }

            const data = new FormData();
            data.append('endpoint', endpoint);
            //
            // // Send a request to the server to mark the notification as read.
            // fetch(`/notifications/${notification.data.id}/dismiss`, {
            //     method: 'POST',
            //     body: data
            // })
        }
    };

    SW.init();
})();
