<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
	// return view('login');
	return view('welcome');
});
Route::get('register', function() {
	return view('register');
});
Route::get('login', function() {
	return view('login');
});
Route::get('forget', function() {
	return view('forget');
});
Route::get('account', function() {
	return view('account');
});
Route::get('complete-account', function() {
	return view('complete-account');
});
Route::get('me', function() {
	return view('me');
});
Route::get('gold', function() {
	return view('gold');
});
Route::get('buy-gold', function() {
	return view('buy-gold');
});
Route::get('transfer-gold', function() {
	return view('transfer-gold');
});
Route::get('home', function() { 
	return view('home');
});
Route::get('home2', function() { 
	return view('home2');
});
Route::get('qr-code', function() {
	return view('qr-code');
});


Route::get('/clear-cache', function() {
	$exitCode = Artisan::call('cache:clear');
	$exitCode = Artisan::call('view:clear');
	$exitCode = Artisan::call('route:clear');
	return 'Application cache cleared';
}); 