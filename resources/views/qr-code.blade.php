@extends('layout/main')

@section('title', 'Home - Laravel')

@section('container')
    <div class="main-container" style="margin-bottom:250px;">  
        <h3 class="text-center text-white p-2 m-3 bg-warning">REGISTER ACCOUNT</h3>
        <div class="d-flex justify-content-start m-3">
            <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ffbf00;color:#848e96;"><i class="fas fa-chevron-left"></i></button>
        </div>
        <div class="card p-3 m-3 card-red"> 
            <div class="card-body row text-white">   
                <style>
                    .carousel-caption {
                        position: relative;
                        left: 0;
                        top: 0;
                    }

                    @media screen and (min-width: 350px){
                        .carousel-caption {
                            display: block; 
                            padding: 5px;
                            width:100%;
                            margin: 0 auto; 
                        }
                        .carousel-caption .h6{ 
                            margin: 0 auto;
                        }
                    }
                </style>

                <div class="bd-example" style="margin:0 auto;"> 
                    <div id="carouselExampleCaptions2" class="carousel slide" data-ride="carousel" style="width:200px;"> 
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{asset('img/qr.png')}}" class="d-block w-100" alt="...">
                                <a href="javascript:void(0)" class="carousel-caption d-none d-lg-block d-xl-block btn btn-copy text-white font-weight-bold mb-4" style="text-decoration: none;">
                                    <span class="h6">QR CODE</span> 
                                </a>
                                <a href="javascript:void(0)" class="carousel-caption d-lg-none d-xl-none btn btn-copy text-white font-weight-bold mb-4" style="text-decoration: none;">
                                    <span class="h6">QR CODE</span> 
                                </a>
                            </div>
                            <div class="carousel-item">
                                <img src="https://img.icons8.com/dusk/512/000000/friends.png" class="d-block w-100" alt="...">
                                <a href="javascript:void(0)" class="carousel-caption d-none d-md-block btn btn-copy text-white font-weight-bold mb-4" style="text-decoration: none;">
                                    <span class="h6">MEMBER ACCOUNT</span> 
                                </a>
                                <a href="javascript:void(0)" class="carousel-caption d-lg-none d-xl-none btn btn-copy text-white font-weight-bold mb-4" style="text-decoration: none;">
                                    <span class="h6">MEMBER ACCOUNT</span> 
                                </a>
                            </div> 
                            <div class="carousel-item">
                                <img src="https://img.icons8.com/color/512/000000/group.png" class="d-block w-100" alt="...">
                                <a href="javascript:void(0)" class="carousel-caption d-none d-md-block btn btn-copy text-white font-weight-bold mb-4" style="text-decoration: none;">
                                    <span class="h6">CHILD ACCOUNT</span> 
                                </a>
                                <a href="javascript:void(0)" class="carousel-caption d-lg-none d-xl-none btn btn-copy text-white font-weight-bold mb-4" style="text-decoration: none;">
                                    <span class="h6">CHILD ACCOUNT</span> 
                                </a>
                            </div> 
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </div>  
        </div> 
    </div> 
@endsection