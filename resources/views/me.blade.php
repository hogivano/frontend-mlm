@extends('layout/main')

@section('title', 'Home - Laravel')

@section('container')
    <div class="main-container m-3" style="margin-bottom:250px !important;">  
        <h3 class="text-center text-white p-2 bg-warning card-red">Multitasking</h3> 
        <div class="d-flex justify-content-start">
            <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ffbf00;color:#848e96;"><i class="fas fa-chevron-left"></i></button>
        </div>
        <div class="card p-3 my-3 card-red" > 
            <div class="card-body row text-white">
                <div class="col-4"><img src="{{asset('img/user.png')}}" width="80" class="rounded-circle"></div>
                <div class="col-8">
                    <h4> Name </h4>
                    <span> Status : PENDING </span><br/>
                    <span> Tier : NORMAL </span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-4"> 
                    <div class="card text-center card-red me-menu-top">
                        <div class="card-body padding-5">
                            <span>$0.00</span>
                            <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                            <span style="font-size: 18px">Reality</span>                            
                        </div>
                    </div>
                </div> 
                <div class="col-4 "> 
                    <div class="card text-center card-red me-menu-top">
                        <div class="card-body padding-5">
                            <span>$0.00</span>
                            <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                            <span style="font-size: 18px">Soul</span>                            
                        </div>
                    </div>
                </div>
                <div class="col-4"> 
                    <div class="card text-center card-red me-menu-top">
                        <div class="card-body padding-5">
                            <span>$0.00</span>
                            <hr style="border: 0;
                            height: 1px;
                            background: #333;
                            background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                            background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                            <span style="font-size: 18px">Power</span>                            
                        </div>
                    </div>
                </div> 
            </div> 
        </div>
        <div class="card p-3 my-3 card-red" style="font-size: 15px;"> 
            <div class="row"> 
                <div class="col-4"> 
                    <a href="" class="card card-red me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-users" style="font-size: 45px;"></span>
                            <span>Inventory Center</span>                        
                        </div>
                    </a>
                </div> 
                <div class="col-4"> 
                    <a href="" class="card card-red me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-handshake" style="font-size: 45px;"></span>
                            <span>Invite</span>                        
                        </div>
                    </a>
                </div>
                <div class="col-4"> 
                    <a href="" class="card card-red me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-shield-alt pt-2" style="font-size: 45px;"></span>
                            <span>Security</span>                        
                        </div>
                    </a>
                </div> 
                <div class="col-4 mt-2"> 
                    <a href="" class="card card-red me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-university" style="font-size: 45px;"></span><br/>
                            <span>Bank Details</span>                        
                        </div>
                    </a>
                </div>
                <div class="col-4 mt-2"> 
                    <a href="" class="card card-red me-menu-bottom">
                        <div class="card-body text-center padding-5">
                            <span class="fas fa-users" style="font-size: 45px;"></span>
                            <span>Group</span>                        
                        </div>
                    </a>
                </div> 
            </div> 
        </div>
    </div>
@endsection