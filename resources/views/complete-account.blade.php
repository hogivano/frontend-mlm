@extends('layout/home')

@section('title', 'Home - Laravel')

@section('container')
<div class="main-container" style="margin-bottom:150px;">  
    <div class="d-flex justify-content-start">
        <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ffbf00;color:#848e96;"><i class="fas fa-chevron-left"></i></button>
    </div>
    <div class="card-red auth-layout mt-3">                      
        <div class="my-3 py-2 py-3 text-success">
            <p class="white-title text-center text-success" style="font-size:40px;">
                90%
            </p>
            <div class="p-1 white-title text-center text-success">
                <h3>Pending Verification</h3>
            </div>
        </div>  
    </div>
</div>
@endsection