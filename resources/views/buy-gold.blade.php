@extends('layout/main')

@section('title', 'Home - Laravel')

@section('container')
    <div class="main-container m-3" style="margin-bottom:150px;">  
        <h3 class="text-center text-white p-2 bg-warning">BUY GOLD BAR</h3>
        <div class="d-flex justify-content-start">
            <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ffbf00;color:#848e96;"><i class="fas fa-chevron-left"></i></button>
        </div>
        <div class="card p-3 my-3" style="background: rgba(254, 193, 7, .45);"> 
            <div class="card-body text-white"> 
                <div class="row">
                    <div class="col">
                        <img src="{{asset('img/gold.png')}}" class="img-thumbnail" style="background: transparent;">
                    </div>
                    <div class="col text-center">
                        <h1>20</h1><h2>GOLD BAR</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-3">
                        <form method="POST" action="" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate"> 
                            <div class="form-group">
                                <label class="control-label">PAYMENT METHOD :</label>
                                <select class="custom-select" style="border:3px solid #000;padding: 6px 10px;">
                                    <option selected>Please select</option>
                                    <option value="1">Marketing Reward</option>
                                    <option value="2">Return Wallet</option>
                                    <option value="3">Restore Reward</option>
                                    <option value="4">BANK Wire</option>
                                    <option value="5">USDT</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">GOLD BAR :</label>
                                 <input type="text" class="form-control" id="exampleFormControlInput1">
                            </div>
                            <div class="form-group">
                                <label class="control-label">CURRENT WALLET PASSWORD :</label>
                                 <input type="text" class="form-control" id="exampleFormControlInput1">
                            </div>
                            <div class="form-group">
                                <button type="button" id="submit-login-btn" class="btn btn-warning btn-block uppercase" style="background: rgba(255, 199, 31, 1);color:#fff;border:none;">
                                    <span>CONFIRM</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>  
        </div> 
        <div class="card p-3 my-3" style="background: rgba(254, 193, 7, .45);"> 
            <div class="card-body text-white"> 
                <h4>BUY RECORD</h4>
                <hr style="border: 0;
                height: 2px;
                background: #333;
                background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -moz-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -ms-linear-gradient(left, #ccc, #333, #ccc);
                background-image: -o-linear-gradient(left, #ccc, #333, #ccc);">
                <table class="table text-white">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">From</th>
                            <th scope="col">To</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>test01</td>
                            <td>test02</td>
                            <td>20.00</td>
                            <td>2020-01-20</td>
                        </tr> 
                    </tbody>
                </table>
            </div>  
        </div> 
    </div>
    @endsection 