<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
    <meta content=" " name="description" />
    <meta content=" " name="author" />  
    <link rel="stylesheet" href="<?=asset('css/user_coreceff.css?id=468f82d64786fe1f5902');?>"> 
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet" id="owl-carousel-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" rel="stylesheet" id="owl-carousel-theme-css">
    <link href="https://fonts.googleapis.com/css2?family=Audiowide&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/18.1.3/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/18.1.3/css/dx.light.css" />
    <script type="text/javascript" src="https://cdn3.devexpress.com/jslib/18.1.3/js/dx.all.js"></script>
    <link href="{{ asset('css/style-new.css') }}" rel="stylesheet" id="responsive-css">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" id="responsive-css">
    <style>

        html > body {
            padding: 0;
            margin: 0;
        }
        #floating-btn {
            z-index: 9;
            width: 70px; 
            height: 70px; 
            border-radius: 50%; 
            position: fixed; 
            background-color: red; 
            right: 20px;
            bottom: 20px;
            padding: 3px 10px;
        }
        #floating-btn i {
            font-size: 30px;
            color: white;
        }
        #floating-btn:hover{
            cursor: pointer;
        }

        .menu-floating {
            position: fixed; bottom: 90px; right: 30px; z-index: 9
        }

        .menu-floating .menu-item {
            margin: 10px 0;
            height: 50px;
            width: 50px;
            border-radius: 50%; 
            border:2px solid red;
            background-color: transparent;
            box-shadow: inset 0 0 20px red; 
            color: white;
        }

        .menu-floating .menu-item i {
            font-size: 30px;
        }
    </style>
</head>
<body>
    <div class="main-container m-3" style="margin-bottom:150px;"> 
        <div class="auth-layout mt-3 card-red w-100" style="padding: 10px 20px;">
            <div>
                <div id="body-alert-container">
                </div>                        
                <div class="my-3">
                    <p class="white-title text-center">
                        Member Account
                    </p>
                    <div class="p-1">
                        <form method="POST" action="" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate"><input name="_token" type="hidden" value="AXDoXw9Rn8fGHinKTrGDo5KYkyN0uY022XTxJrtE">
                            <div class="form-group">
                                <label class="control-label">Introducer :</label>
                                <input class="form-control" name="username" type="text" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Username :</label>
                                <input class="form-control" name="username" type="text" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email :</label>
                                <input class="form-control" name="username" type="text" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Country :</label>
                                <select class="custom-select form-control">
                                	<option selected>Select Country</option>
                                	<option value="37">Cambodia</option>
                                    <option value="45" selected="selected">China</option>
                                    <option value="98">Hong Kong</option>
                                    <option value="101">India</option>
                                    <option value="102">Indonesia</option>
                                    <option value="133">Malaysia</option>
                                    <option value="197">Singapore</option>
                                    <option value="213">Taiwan (Province of China)</option>
                                    <option value="216">Thailand</option>
                                    <option value="237">Viet Nam</option> 
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Contact number</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <select class="form-control" id="contact-country-selector" name="contact_country_id"><option value="37">+855</option>
                                            <option value="45" selected="selected">+86</option>
                                            <option value="98">+852</option>
                                            <option value="101">+91</option>
                                            <option value="102">+62</option>
                                            <option value="133">+60</option>
                                            <option value="197">+65</option>
                                            <option value="213">+886</option>
                                            <option value="216">+66</option>
                                            <option value="237">+84</option>
                                        </select>
                                    </div>
                                    <input class="form-control number-input" id="contact-number-input" name="contact_number" type="text" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">PASSWORD :</label>
                                <input class="form-control" name="password" type="password" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">PASSWORD CONFIRMATION :</label>
                                <input class="form-control" name="password" type="password" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">WALLET PASSWORD :</label>
                                <input class="form-control" name="password" type="password" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">WALLET PASSWORD CONFIRMATION :</label>
                                <input class="form-control" name="password" type="password" value="">
                            </div> 
                            <div class="text-center text-white">Already have a account? <a href="{{ url('login') }}" style="color:#fff;font-weight: bold;">LOGIN</a></div>
                            <div class="d-flex justify-content-center">
                                <button type="button" id="submit-login-btn" class="btn btn-copy uppercase text-center" style="width: 200px;">
                                    <span>REGISTER</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <script> 
    </script> 
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="{{ asset('dist/mfb.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#floating-btn').on('click', function(){
                console.log("oke");
                if ($('.menu-floating').hasClass('d-none')){
                    $('.menu-floating').removeClass('d-none');
                    console.log('true')
                } else {
                    $('.menu-floating').addClass('d-none'); 
                }
            });
        });
    </script>
</body> 
</html>