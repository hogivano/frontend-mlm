<div class="row fixed-bottom container mx-auto mb-3 menus div-menus">
        <div class="col-3 p-0"> 
            <a href="{{ url('home') }}" class="btn pt-2 circle-menu" style=""><i class="fas fa-home"></i><br/><small>HOME</small></a>  
        </div>
        <div class="col-3 p-0"> 
            <a href="{{ url('home') }}" class="btn pt-2 px-3 circle-menu" style=""><i class="fas fa-dollar-sign"></i><br/><small>HIRE</small></a>  
        </div>
        <div class="col-3 p-0"> 
            <a href="{{ url('qr-code') }}" class="btn pt-2 circle-menu" style=""><i class="fas fa-user-plus"></i><br/><small>INVITE</small></a>  
        </div>
        <div class="col-3 p-0"> 
            <a href="{{ url('me') }}" class="btn pt-2 px-4 circle-menu" style=""><i class="fas fa-user"></i><br/><small>ME</small></a>  
        </div>
    </div> 