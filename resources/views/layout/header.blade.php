<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Information</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="AXDoXw9Rn8fGHinKTrGDo5KYkyN0uY022XTxJrtE" />
    <meta content=" " name="description" />
    <meta content=" " name="author" />
    <!--begin::Fonts -->
    <link rel="apple-touch-icon" href="">
    <link rel="shortcut icon" href="">
    <link rel="manifest" href="manifest.json">
    <link rel="stylesheet" href="<?=asset('css/user_coreceff.css?id=468f82d64786fe1f5902');?>"> 
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <style>
        html > body {
            background-image: url('img/bg2.jpg');
            background-position: center center;
            background-attachment: fixed;
            background-repeat: no-repeat;
            background-size: cover;
            min-height: 100vh;
            font-family: 'Roboto', sans-serif;
        }
        .loader {
            position: relative;
            width: 100vw;
            height: 100vh;
            background: #000;
        }
        .loader > div {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -116px;
            margin-left: -75px;
        }
        .loader > div > img {
            width: 150px;
            clear: both;
        }
        .loader > img.logo {
            width: 100%;
            position: absolute;
            margin: 100px auto 0 auto;
            text-align: center;
            max-width: var(--max-width);
            transform: translate(-50%, 0%);
        }
        .loader > img.characters {
            width: 100%;
            max-width: var(--max-width);
            position: absolute;
            margin: 0 auto;
            text-align: center;
            bottom: 0;
            transform: translate(-50%, 0%);
        }  
        

        .scrolling-wrapper {
            overflow-x: auto;
            overflow-y: hidden;
            white-space: nowrap; 
            -webkit-overflow-scrolling: touch;
        }
        .scrolling-wrapper > .card {
            display: inline-block;            
        }
    </style>
    <script src="manup.min.js" type="text/javascript"></script>
</head>
<body>
    <div class="loader">
        <img src="img/logo.png" class="logo">
        <div>
            <br>
            <img src="img/loading.gif">
        </div>
        <img src="img/char.png" class="characters">
    </div>
    <div class="main-container"> 
        <a href="login.html">
            <img src="img/logo.png" class="img-fluid">
        </a>
    </div>
    <div class="row fixed-bottom container mx-auto mb-3" style="max-width: 400px;background: rgba(255, 168, 0, .45);border-top-right-radius: 25%;border-top-left-radius: 25%;padding-top:30px;border-bottom-right-radius: 15px;border-bottom-left-radius: 15px;padding-top:30px;padding-bottom: 10px;">
        <div class="col m-1 ml-3 p-0"> 
            <button class="btn btn-warning pt-2" style="border-top-right-radius: 25%;border-top-left-radius: 25%;border-bottom-right-radius: 5%;border-bottom-left-radius: 5%;"><i class="fas fa-home" style="font-size:26px;"></i><br/>HOME</button>  
        </div>
        <div class="col m-1 p-0"> 
            <button class="btn btn-warning pt-2 px-3" style="border-top-right-radius: 25%;border-top-left-radius: 25%;border-bottom-right-radius: 5%;border-bottom-left-radius: 5%;"><i class="fas fa-dollar-sign" style="font-size:26px;"></i><br/>HIRE</button>  
        </div>
        <div class="col m-1 p-0"> 
            <button class="btn btn-warning pt-2" style="border-top-right-radius: 25%;border-top-left-radius: 25%;border-bottom-right-radius: 5%;border-bottom-left-radius: 5%;"><i class="fas fa-user-plus" style="font-size:26px;"></i><br/>INVITE</button>  
        </div>
        <div class="col m-1 p-0"> 
            <button class="btn btn-warning pt-2 px-4" style="border-top-right-radius: 25%;border-top-left-radius: 25%;border-bottom-right-radius: 5%;border-bottom-left-radius: 5%;"><i class="fas fa-user" style="font-size:26px;"></i><br/>ME</button>  
        </div>
    </div> 