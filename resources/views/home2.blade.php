@extends('layout/main')

@section('title', 'Home - Laravel')

@section('container')
<div class="container scrolling-wrapper mt-5" style="margin-bottom:200px;max-width:1200px;"> 
    <?php for($i=0;$i<5;$i++):?>
        <div class="card mb-2" style="border:2px solid #000;background: url('../public/img/test2.png');box-shadow: inset 0 0 20px #ffbf00;">
            <div class="card-body row">
                <div class="col-12" style="color:#848e96;"> 
                    <span class="h4 card-title mt-2">Name </span><small>(ACTIVE)</small>
                    <table border="0">
                        <tr>
                            <td>PROFIT</td>
                            <td> : </td>
                            <td>10%-30%</td>
                        </tr>
                        <tr>
                            <td>PRICE</td>
                            <td> : </td>
                            <td>20 - 200USD</td>
                        </tr>
                        <tr>
                            <td>GOLD BAR</td>
                            <td> : </td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>TIME</td>
                            <td> : </td>
                            <td>12:00</td>
                        </tr>
                        <tr>
                            <td colspan="3"><button class="btn px-5 font-weight-bold border-0 mx-auto" style="color:#848e96;box-shadow: inset 0 0 20px #ffbf00;">HIRE +</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div> 
    <?php endfor;?>   
</div> 
@endsection