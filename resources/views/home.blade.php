@extends('layout/main')

@section('title', 'Home - Laravel')

@section('container')
    <?php
        $listName = ["01.Captain Marvel", "02.Black Widow", "03.THOR", "04.HULK", "05.Black Panther", "06.Iron Man", "05.Captain America"];
        $listProfit = ["10%-30%", "12%-20%", "12%-10%", "10%-30%", "10%-30%", "10%-30%", "10%-30%"];
        $listPrice = ["10-100", "100-110", "10", "10%-30%", "10%-30%", "10%-30%", "10%-30%"];
        $listSoul = ["2", "3", "4", "5", "6", "10%-30%", "10%-30%"];
        $listTime = ["09.00", "10.00", "11.00", "12.00", "13.00", "10%-30%", "10%-30%"];

        $listStatus = ["ACTIVE", "PENDING", "SUSPEND", "BLOCKED", "ACTIVE", "ACTIVE", "ACTIVE"];
        $listPhoto = ["img/bgcm01.png", "img/bgbw1.png", "img/bgthor1.png", "img/bghulk1.png", "img/bgbp1.png", "img/bgiron1.png", "img/bgcam1.png"];
        $lc = ["card-yellow", "card-red", "card-green"];
    ?>
@if(Agent::isMobile())
<div style="height: 100vh; width: 100%; position: relative">
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-wrap="false">
      <div class="carousel-inner wh-100">
        @for($i = 0; $i < 7; $i++)
        @if($i == 0)
        <div class="carousel-item active wh-100" style="position: relative;">
        @else
        <div class="carousel-item wh-100" style="position: relative;">
        @endif
            <img src="{{ $listPhoto[$i] }}" style="object-fit: cover;" class="wh-100" alt="Card image ca" >
                    <div style="position: absolute; z-index: 999;z-index: 999;bottom: 30px;left: 30px;">
                        <span class="h4 card-title mt-2 text-success">{{ $listName[$i] }} </span>
                        <p class="text-success">
                            <small>({{ $listStatus[$i] }})</small>
                        </p>
                        <table border="0" class="text-success">
                            <tr >
                                <td>PROFIT</td>
                                <td> : </td>
                                <td style="color:#ff0dfb">{{ $listProfit[$i] }} </td>
                            </tr>
                            <tr>
                                <td>PRICE</td>
                                <td> : </td>
                                <td  style="color:#ff0dfb">{{ $listPrice[$i] }} </td>
                            </tr>
                            <tr>
                                <td>S O U L</td>
                                <td> : </td>
                                <td  style="color:#ff0dfb">{{ $listSoul[$i] }}</td>
                            </tr>
                            <tr>
                                <td class="text-success">TIME</td>
                                <td> : </td>
                                <td  style="color:#ff0dfb">{{ $listTime[$i] }}</td>
                            </tr>
                            <tr>
                                <td colspan="3"><button class="btn px-5 font-weight-bold border-0 mx-auto btn-copy" style="padding: 5px 10px!important">ASSAMBLE <i class="fas fa-angle-double-right"></i></button></td>
                            </tr>
                        </table>
                    </div>
        </div>
        @endfor
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</div>
@else
<div class="container owl-carousel mt-5" style="margin-bottom:200px;max-width:1200px;">
    <?php for($i=0;$i<7;$i++):?>
        <div class="card mb-2 card-red" style="width: 300px;">
            <div class="card-body row">
                <div class="col-12 text-success" style="padding-left: 10px; padding-right: 3px">
                    <img class="rounded d-block mx-auto mb-3" src="{{ asset($listPhoto[$i]) }}" alt="Card image ca" style="width:100%;margin-left: 0px !important; margin-right: 0px !important">
                    <span class="h4 card-title mt-2 text-success">{{ $listName[$i] }} </span>
                    <p><small>({{ $listStatus[$i] }})</small></p>
                    <table border="0">
                        <tr >
                            <td style="color: blue; text-shadow: 2px 2px 5px red">PROFIT</td>
                            <td> : </td>
                            <td style="color:#ff0dfb">{{ $listProfit[$i] }} </td>
                        </tr>
                        <tr>
                            <td>PRICE</td>
                            <td> : </td>
                            <td  style="color:#ff0dfb">{{ $listPrice[$i] }} </td>
                        </tr>
                        <tr>
                            <td>S O U L</td>
                            <td> : </td>
                            <td  style="color:#ff0dfb">{{ $listSoul[$i] }}</td>
                        </tr>
                        <tr>
                            <td class="text-success">TIME</td>
                            <td> : </td>
                            <td  style="color:#ff0dfb">{{ $listTime[$i] }}</td>
                        </tr>
                        <tr>
                            <td colspan="3"><button class="btn px-5 font-weight-bold border-0 btn-copy" style="padding: 5px 10px!important">WAR!! <i class="fas fa-angle-double-right"></i> </button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    <?php endfor;?>
</div>
@endif
@endsection
@section('script')
    <script>
        $(document).ready(function(){
          $(".owl-carousel").owlCarousel();
        });
    </script>
@endsection
