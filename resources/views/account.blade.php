@extends('layout/home')

@section('title', 'Home - Laravel')

@section('container')
<div class="main-container" style="margin-bottom:150px;">  
    <div class="d-flex justify-content-start">
        <button class="btn text-white" onclick="window.history.back();" style="box-shadow: inset 0 0 25px #ffbf00;color:#848e96;"><i class="fas fa-chevron-left"></i></button>
    </div>
    <div class="auth-layout mt-3 card-red w-100" style="padding: 10px 20px;">
        <div>                    
            <div class="my-3">
                <p class="white-title text-center">
                    Information
                </p>
                <div class="p-1">
                    <form method="POST" action="" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate"> 
                        <div class="form-group">
                            <label class="control-label">Fullname :</label>
                            <input class="form-control" name="username" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">ID No :</label>
                            <input class="form-control" name="username" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Wechat ID :</label>
                            <input class="form-control" name="username" type="text" value="">
                        </div> 
                        <div class="form-group">
                            <label class="control-label">NEW PASSWORD :</label>
                            <input class="form-control" name="password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">PASSWORD CONFIRMATION :</label>
                            <input class="form-control" name="password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">WALLET PASSWORD :</label>
                            <input class="form-control" name="password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">NEW WALLET PASSWORD :</label>
                            <input class="form-control" name="password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">NEW WALLET PASSWORD CONFIRMATION :</label>
                            <input class="form-control" name="password" type="password" value="">
                        </div>   
                        <div class="form-group">
                            <label class="control-label">Country :</label>
                            <select class="custom-select form-control">
                                <option selected>Select Country</option>
                                <option value="1">Indonesia</option>
                                <option value="2">Malaysia</option>
                                <option value="3">Singapura</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Bank :</label>
                            <select class="custom-select form-control">
                                <option selected>Select Bank</option>
                                <option value="1">BNI</option>
                                <option value="2">BRI</option>
                                <option value="3">BCA</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Bank Account Number :</label>
                            <input class="form-control" name="username" type="text" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Bank Account Holder Name :</label>
                            <input class="form-control" name="username" type="text" value="">
                        </div>  
                        <div class="form-group">
                            <label class="control-label">Swift Code :</label>
                            <input class="form-control" name="username" type="text" value="">
                        </div> 
                        <div class="d-flex justify-content-center">
                            <button type="button" id="submit-login-btn" class="btn btn-copy uppercase" style="width: 200px">
                                <span>NEXT</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
    </div>
</div>
@endsection