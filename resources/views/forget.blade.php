@extends('layout/home')

@section('title', 'Home - Laravel')

@section('container')
    <div class="main-container m-3" style="margin-bottom:150px;">  
        <div class="card-red auth-layout mt-3">
            <div>
                <div id="body-alert-container">
                </div>                        
                <div class="m-3">
                    <p class="white-title text-center">
                        Forget Password
                    </p>
                    <div class="p-1">
                        <form method="POST" action="" accept-charset="UTF-8" role="form" autocomplete="off" id="login-form" novalidate="novalidate"><input name="_token" type="hidden" value="AXDoXw9Rn8fGHinKTrGDo5KYkyN0uY022XTxJrtE">
                            <div class="form-group">
                                <label class="control-label">Username :</label>
                                <input class="form-control" name="username" type="text" value="">
                            </div> 
                            <div class="form-group text-right">
                                <a href="{{ url('login') }}" class="link" style="color:#fff;font-weight:bold;text-decoration: none;">Back to login</a>
                            </div> 
                            <div class="d-flex justify-content-center">
                                <button type="button" id="submit-login-btn" class="btn btn-copy uppercase" style="width: 200px">
                                    <span>Reset Password</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
@endsection